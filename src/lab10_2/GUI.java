package lab10_2;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class GUI extends JFrame {
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 300;
	
	private final static Color RED_COLOR = Color.RED;
	private final static Color GREEN_COLOR = Color.GREEN;
	private final static Color BLUE_COLOR = Color.BLUE;
	
	private JFrame frame;
	private JPanel panel1,panel2;
	private JRadioButton btnR,btnG,btnB;
	
	public GUI(){
		setSize(FRAME_WIDTH,FRAME_HEIGHT);
		createButton();
		
		panel1 = new JPanel();
		panel1.setLayout(null);
		panel2 = new JPanel(new GridLayout(1,3));
		
		panel2.add(btnR);
		panel2.add(btnG);
		panel2.add(btnB);
		
		add(panel1,BorderLayout.CENTER);
		add(panel2,BorderLayout.SOUTH);
	}

	private void createButton() {
		// TODO Auto-generated method stub
		
		btnR = new JRadioButton("Red");
		btnR.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	panel1.setBackground(RED_COLOR);
            }});
		
		btnG = new JRadioButton("Green");
		btnG.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	panel1.setBackground(GREEN_COLOR);
            }});
		
		btnB = new JRadioButton("Blue");
		btnB.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	panel1.setBackground(BLUE_COLOR);
            }});
	
		
		ButtonGroup group = new ButtonGroup();
		group.add(btnR);
		group.add(btnG);
		group.add(btnB);
		
	}
}
