package lab10_7;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JFrame;


public class Testing {
	BankAccount bank;
	GUI frame;
	ActionListener listener;
	ArrayList<String> ans;
	String result;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Testing();
	}
	
	public Testing(){
		frame = new GUI();
		bank = new BankAccount(0);
		ans = new ArrayList<String>();

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Lab10_7");
		frame.setVisible(true);
		
		frame.getBtnDeposit().addActionListener(listener);
		frame.getBtnWithdraw().addActionListener(listener);
		
		frame.setListener(new ActionListener() {
	        public void actionPerformed(ActionEvent event){
	        	double amount = Double.parseDouble(frame.getInput());
	        	bank.deposit(amount);
	        	result = ("Deposit  : "+ amount+"    balance : " + bank.getBalance());
	        	ans.add(result);
	        	frame.setResult(getResult());
	        	}});

		frame.setListener2(new ActionListener() {
	        public void actionPerformed(ActionEvent event){
	        	double amount = Double.parseDouble(frame.getInput());
	        	bank.withdraw(amount);
	        	result = ("Withdraw : "+ amount+"  balance : " + bank.getBalance());
	        	ans.add(result);
	        	frame.setResult(getResult());
	        	}});
	}
	
	public String getResult(){
		String print = ""; 
		for (int i = 0; i< ans.size();i++){
			print += ans.get(i)+ "\n";
		}
		return print;
	}
}
