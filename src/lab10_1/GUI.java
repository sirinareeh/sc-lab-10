package lab10_1;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GUI extends JFrame {
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 300;
	
	private final static Color RED_COLOR = Color.RED;
	private final static Color GREEN_COLOR = Color.GREEN;
	private final static Color BLUE_COLOR = Color.BLUE;
	
	private JFrame frame;
	private JButton btnR,btnG,btnB;
	private JPanel panel1,panel2;
	
	public GUI(){
		setSize(FRAME_WIDTH,FRAME_HEIGHT);
		createButton();
		
		panel1 = new JPanel();
		panel2 = new JPanel(new GridLayout(1,3));
		
		panel2.add(btnR);
		panel2.add(btnG);
		panel2.add(btnB);
		
		add(panel1,BorderLayout.CENTER);
		add(panel2,BorderLayout.SOUTH);
		
	}

	private void createButton() {
		// TODO Auto-generated method stub
		btnR = new JButton("Red");
		btnR.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	panel1.setBackground(RED_COLOR);
            }});
		btnG = new JButton("Green");
		btnG.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	panel1.setBackground(GREEN_COLOR);
            }});
		
		btnB = new JButton("Blue");
		btnB.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	panel1.setBackground(BLUE_COLOR);
            }});
	
		
	}
	
	
	
}


