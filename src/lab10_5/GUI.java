package lab10_5;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class GUI extends JFrame {
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 300;
	
	private final static Color RED_COLOR = Color.RED;
	private final static Color GREEN_COLOR = Color.GREEN;
	private final static Color BLUE_COLOR = Color.BLUE;
	
	private JFrame frame;
	private JMenu file,edit,color;
	private JMenuItem red,green,blue,exit;
	private JMenuBar menu;
	private JPanel panel;
	
	public GUI(){
		setSize(FRAME_HEIGHT,FRAME_WIDTH);
		
		file = new JMenu("File");
		edit = new JMenu("Edit");
		color = new JMenu("Setting Color");
		
		exit = new JMenuItem("Exit");
		red = new JMenuItem("Red");
		green = new JMenuItem("Green");
		blue = new JMenuItem("Blue");
		
		file.add(exit);
		edit.add(color);
		color.add(red);
		color.add(green);
		color.add(blue);

		menu = new JMenuBar();
		menu.add(file);
		menu.add(edit);
		setJMenuBar(menu);
		
		panel = new JPanel();
		add(panel,BorderLayout.CENTER);
		
		exit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				System.exit(0);	
            }});
		
		red.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	panel.setBackground(RED_COLOR);
            }});
		
		green.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	panel.setBackground(GREEN_COLOR);
            }});
		
		blue.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	panel.setBackground(BLUE_COLOR);
            }});

	}

}
