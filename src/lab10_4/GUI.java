package lab10_4;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GUI extends JFrame {
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 300;
	
	private final static Color RED_COLOR = Color.RED;
	private final static Color GREEN_COLOR = Color.GREEN;
	private final static Color BLUE_COLOR = Color.BLUE;
	
	private JFrame frame;
	private JButton btn;
	private JComboBox<String> comboBox;
	private JPanel panel1,panel2;
	
	public GUI(){
		setSize(FRAME_WIDTH,FRAME_HEIGHT);
		
		comboBox = new JComboBox<String>();
		comboBox.addItem("Red");
		comboBox.addItem("Green");
		comboBox.addItem("Blue");
		
		btn = new JButton("OK");
		btn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					String r = ("Red");
					if(r == comboBox.getSelectedItem()){
						panel1.setBackground(RED_COLOR);
					}
					String g = ("Green");
					if(g == comboBox.getSelectedItem()){
						panel1.setBackground(GREEN_COLOR);
					}
					String b = ("Blue");
					if(b == comboBox.getSelectedItem()){
						panel1.setBackground(BLUE_COLOR);
					}
					
				}
			}
		);
		
		
		
		panel1 = new JPanel();
		panel2 = new JPanel(new GridLayout(1,2));
		
		panel2.add(comboBox);
		panel2.add(btn);
		
		
		
		
		add(panel1,BorderLayout.CENTER);
		add(panel2,BorderLayout.SOUTH);
		
		
	}
	
}
