package lab10_6;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

public class Testing {
	BankAccount bank;
	GUI frame;
	ActionListener listener;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Testing();
		
	}
	public Testing(){
		frame = new GUI();
		bank = new BankAccount(0);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Lab10_6");
		frame.setVisible(true);
		
		frame.getBtnDeposit().addActionListener(listener);
		frame.getBtnWithdraw().addActionListener(listener);
		
		
		frame.setListener(new ActionListener() {
	        public void actionPerformed(ActionEvent event){
	        	double amount = Double.parseDouble(frame.getInput());
	        	bank.deposit(amount);
	        	frame.getLabel().setText("Balance : " + bank.getBalance());
	        	}});

		frame.setListener2(new ActionListener() {
	        public void actionPerformed(ActionEvent event){
	        	double amount = Double.parseDouble(frame.getInput());
	        	bank.withdraw(amount);
	        	frame.getLabel().setText("Balance : " + bank.getBalance());
	        	}});
	
	}   
    
}
