package lab10_3;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GUI extends JFrame {
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 300;
	
	private final static Color RED_COLOR = Color.RED;
	private final static Color GREEN_COLOR = Color.GREEN;
	private final static Color BLUE_COLOR = Color.BLUE;
	
	private final static Color YELLOW_COLOR = Color.YELLOW;
	private final static Color MAGENTA_COLOR = Color.MAGENTA;
	private final static Color CYAN_COLOR = Color.CYAN;
	private final static Color BLACK_COLOR = Color.BLACK;
	
	
	private JCheckBox btnR,btnG,btnB;
	private JPanel panel1,panel2;
	private ActionListener ltn;
	
	public GUI(){
		setSize(FRAME_WIDTH,FRAME_HEIGHT);
		
		class Listener implements ActionListener {

			@Override
			public void actionPerformed(ActionEvent e) {

				if(btnR.isSelected()){
					panel1.setBackground(RED_COLOR);
				}
				if(btnG.isSelected()){
					panel1.setBackground(GREEN_COLOR);
				}
				if(btnB.isSelected()){
					panel1.setBackground(BLUE_COLOR);
				}
				if(btnR.isSelected() & btnG.isSelected()){
					panel1.setBackground(YELLOW_COLOR);
				}
				if(btnR.isSelected() & btnB.isSelected()){
					panel1.setBackground(MAGENTA_COLOR);
				}
				if(btnG.isSelected() & btnB.isSelected()){
					panel1.setBackground(CYAN_COLOR);
				}
				if(btnR.isSelected() & btnG.isSelected() & btnB.isSelected()){
					panel1.setBackground(BLACK_COLOR);
				}
			}
		}
		
		ltn = new Listener();

		btnR = new JCheckBox("Red");
		btnR.addActionListener(ltn);
		btnG = new JCheckBox("Green");
		btnG.addActionListener(ltn);
		btnB = new JCheckBox("Blue");
		btnB.addActionListener(ltn);
		
		panel1 = new JPanel();
		
		panel2 = new JPanel(new GridLayout(1,3));
		
		panel2.add(btnR);
		panel2.add(btnG);
		panel2.add(btnB);
		
		add(panel1,BorderLayout.CENTER);
		add(panel2,BorderLayout.SOUTH);
	}

}
